#!/bin/bash
echo -e "\e[32mAPT\e[5m RAFRAICHISSEMENTS DES PAQUETS\e[0m"
apt update
echo -e "\e[32mAPT\e[5m MISES A JOUR DES PAQUETS\e[0m"
apt upgrade
echo -e "\e[32mAPT\e[5m NETOYAGE DES PAQUETS PLUS UTILISE\e[0m"
apt autoremove
echo -e "\e[32mFLATPAK\e[5m MISES A JOUR FLATPAK\e[0m"
flatpak update
echo -e "\e[32mSNAP\e[5m MISES A JOUR SNAP\e[0m"
snap refresh
echo -e "\e[32mFSTRIM\e[5m FSTRIM / et /home\e[0m"
fstrim -v /
fstrim -v /home/
echo -e "\e[92mFIN DES MISES A JOURS, APPUYEZ SUR ENTREE"
read