#!/bin/bash

url_de_la_video=$1

youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o '~/Vidéos/youtube-dl/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' --write-sub --write-auto-sub --sub-lang fr,en --embed-subs $url_de_la_video